<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrador = User::create([
            'name' => 'Administrador',
            'apePaterno' => 'Hernandez',
            'apeMaterno' => 'Mendoza',
            'email' => 'admin@brahman.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'foto'=> 'profile.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        $usuario = User::create([
            'name' => 'Usuario',
            'apePaterno' => 'Braham',
            'apeMaterno' => 'Restaurante',
            'email' => 'user@brahman.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'foto'=> 'profile.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        $administrador->assignRole('Administrador');
        $usuario->assignRole('Usuario');
    }
}