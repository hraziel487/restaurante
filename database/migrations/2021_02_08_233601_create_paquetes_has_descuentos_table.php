<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaquetesHasDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes_has_descuentos', function (Blueprint $table) {
            $table->unsignedBigInteger('paquetes_id');
            $table->unsignedBigInteger('descuentos_id');
            $table->foreign('paquetes_id')
                ->references('id')
                ->on('paquetes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('descuentos_id')
                ->references('id')
                ->on('descuentos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paquetes_has_descuentos');
    }
}
