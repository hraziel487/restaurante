<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlatillosHasDescuentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('platillos_has_descuentos', function (Blueprint $table) {
            $table->unsignedBigInteger('platillos_id');
            $table->unsignedBigInteger('descuentos_id');
            $table->foreign('platillos_id')
                ->references('id')
                ->on('platillos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('descuentos_id')
                ->references('id')
                ->on('descuentos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platillos_has_descuentos');
    }
}
