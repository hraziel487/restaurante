
@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

@section('content')
<div class="content col-md-12 ml-auto mr-auto">
    <div class="header py-5 pb-7 pt-lg-9">
        <div class="container col-md-10">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-12 col-md-12 ">
                        <h5 class="titulo">{{ __('GALERIA') }}</h5>
                        <div class="container">
                            <div class="row">
                                @foreach ($galeria as $gale)
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <div class="card card-stats">
                                        <img class="img-responsive animacion" src="{{ asset('storage/'. $gale->foto) }}" > 
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>                              
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
