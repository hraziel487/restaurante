@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

@section('content')
<div class="content col-md-12 ml-auto mr-auto">
    <div class="header py-5 pb-7 pt-lg-9">
        <div class="container col-md-10">
               <div class="Titulo-general">
                        <h5 class="subtitulo" style="margin-left: 3%;">{{ __('Restaurante') }}</h5>
                        <h3 class="titulo" style="margin-top: -49px;">{{ __('Menu') }}</h3>                        
                    </div>
            <div class="media">
              <!--<img src="{{ asset('paper/img/menu-1.jpeg') }}" id="img-tamaño" >  --> 
                <div class="media-body">
                  <div id="especialidades"> 
                    <h3 class="titulo-menu mb-1">BEBIDAS</h3>
                    @php
                      $count = 0;
                      $cond = true;
                    @endphp
                    @foreach ($menu as $platillo)
                      @if($count % 5 == 0)
                        @php
                          $cond = !$cond;
                        @endphp
                      @endif
                      @if($cond == true)
                        <p class="platillo" style="text-align:right;"> {{ $platillo->nombre }} ................................................. <small class="precio">${{ $platillo->precio }}</small></p>
                        <small style="color: wheat;position: relative;bottom: 25px;"><p style="text-align:right;">( "{{ $platillo->descripcion }}" )</p></small>
                      @else
                        <p  class="platillo"> {{ $platillo->nombre }} ................................................. <small class="precio">${{ $platillo->precio }}</small></p>
                        <small style="color: wheat;position: relative;bottom: 25px;text-align:left;">( "{{ $platillo->descripcion }}" )</small>
                      @endif
                      @php
                        $count++;
                      @endphp
                    @endforeach
         
                  </div>                
                </div>
              </div>
                  <div ><img style="position: absolute;bottom: 125pc;left: 38pc;" src="{{ asset('paper/img/Menu-2.jpg') }}" id="img-tamaño-2" ></div>
                  <div ><img  style="position: absolute;bottom: 36pc;" src="{{ asset('paper/img/Menu-1.jpeg') }}" id="img-tamaño-2" ></div>
                  <div><img  style="position: absolute;bottom: 65pc;left: 41pc;"src="{{ asset('paper/img/platillos/f210fc51-9c47-47e4-925b-82a401817c02(1)(1).jpg') }}" id="img-tamaño-2" ></div>
                  <div ><img style="position: absolute;bottom: 95pc;" src="{{ asset('paper/img/platillos/IMG-3812 (2).jpg') }}" id="img-tamaño-2" ></div>
                  <div ><img  style="position: absolute;bottom: 2pc;left: 40pc;"src="{{ asset('paper/img/platillos/WhatsApp Image 2020-12-04 at 4.59.27 PM (2).jpeg') }}" id="img-tamaño-2" ></div>
                 
                </div>
                
            </div>
            
        </div>
    </div>
</div>
@endsection