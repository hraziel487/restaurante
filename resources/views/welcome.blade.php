@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])
<style>
    .caja1{
        width: 100%;
        position: relative;
        display: inline-block;
    }

    .titul{
        font-family: 'Montserrat', sans-serif;
        position: absolute;
        top: 35%;
        font-size: 17vh;
        left: 15%;
        color: aliceblue;

    }

    .titul1{
        font-family: 'Montserrat', sans-serif;
        position: absolute;
        top: 48%;
        font-size: 14vh;
        left: 15%;
        color: aliceblue;

    }

    .linea{
        width: 70em;
        height: 10px;
        position: absolute;
        top: 59%;
        left: 16%;
        background: #e4c27b;
    }

    @media (max-width: 1024px) {
    
    .titul{
        font-family: 'Montserrat', sans-serif;
        position: absolute;
        top: 35%;
        font-size: 10vh;
        left: 12%;
        color: aliceblue;
    }

    .titul1{
        font-family: 'Montserrat', sans-serif;
        position: absolute;
        top: 53%;
        font-size: 5vh;
        left: 13%;
        color: aliceblue;
    }

    .linea{
        width: 55em;
        height: 10px;
        position: absolute;
        top: 62%;
        left: 12%;
        background: #e4c27b;

    }

    @media (max-width: 700px) {
    
    .titul{
        font-size: 30px;
        top: 47%;
    }

    .titul1{
        font-size: 27px;
        top: 60%;
    }

    .linea{
        width: 22em;
        height: 5px;
        top: 72%;

    }
}


}
</style>
@section('content')
    <div class="content col-md-12 ml-auto mr-auto">
        <div class="caja1">
            <img src="{{ asset('paper/img/platillos/WhatsApp Image 2020-12-04 at 1.44.53 PM (3).jpg') }}" class="img-fluid mt-5" alt="Responsive image" style="width: 100% !important; border-radius:15px !important; opacity: 0.6 !important;">
            <h1 class="titul">BRAHMAN</h1>
            <h1 class="titul1">CORTES AHUMADOS</h1>
            <div class="linea"></div>
        </div>
        
            <div class="container" style="background: #000000!important">
                    <div class="d-flex justify-content-center mt-3" style="color: white !important">
                        <h2 class="card-title text-center">NUESTROS PAQUETES</h2> 
                    </div>
                <div class="card-deck" style="background: #000000  !important; ">
                    <div class="card text-center" style="background: #000000  !important; ">
                        <img src="{{ asset('paper/img/platillos/steak.png') }}" class="img-fluid" alt="..." style="border-radius: 12px !important">
                            <div class="card-body">
                            <h5 class="card-title" style="color:#e4c27b !important">Steak House</h5>
                            <p class="card-text" style="color: white !important">Costillas bbq, pollo, costilla de res, chorizo, chistorra, puré rustico, arroz primavera, pan pizza, ensalada americana, frijol dallas (4 personas).</p>
                        </div>
                    </div>
                    <div class="card text-center" style="background: #000000  !important; ">
                        <img src="{{ asset('paper/img/platillos/individual.png') }}" class="img-fluid" alt="..." style="border-radius: 12px !important">
                        <div class="card-body">
                        <h5 class="card-title" style="color:#e4c27b !important">Brahman Individual</h5>
                        <p class="card-text" style="color: white !important">Costilla de res, pierna y muslo, frijol, arroz, puré de papa.</p>
                        </div> 
                    </div>
                    <div class="card text-center" style="background: #000000  !important; ">
                        <div class="card text-center" style="background: #000000 !important">
                            <img src="{{ asset('paper/img/platillos/pastas.png') }}" class="img-fluid" alt="..." style="border-radius: 12px !important">
                            <div class="card-body">
                            <h5 class="card-title" style="color:#e4c27b !important">Pastas</h5>
                            <p class="card-text" style="color: white !important">Pastas con cortes de arrachera, pollo, picaña, brisket y costilla de res.</p>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center mt-3" ">
                    <a href="#" class="btn btn-outline-warning btn-lg" role="button" aria-pressed="true" style="color: white !important">VER MÁS</a>
                </div>
                    
            </div>

            <div class="d-flex justify-content-center mt-4" style="color:#e4c27b !important">
                <h2 class="card-title text-center">LOS MEJORES AHUMADOS</h2> 
            </div>

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{ asset('paper/img/platillos/corte1.png') }}" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                        <h1>Mezcla perfecta de sabores y texturas</h2>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('paper/img/platillos/slide2.png') }}" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h1>Mezcla perfecta de sabores y texturas</h2>
                                
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ asset('paper/img/platillos/slide3.png') }}" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h1>Mezcla perfecta de sabores y texturas</h2>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <br>

    </div>
</div>
</div>

@endsection
