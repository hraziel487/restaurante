<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('paper') }}/css/bootstrap.min.css" rel="stylesheet" />
    <title>Document</title>
</head>
<body>
    <div class="card text-center">
        <div class="card-header">
            <h1>Nueva información de contacto</h1>
        </div>
        <div class="card-body">
            <Strong>Nombre:</Strong>
            <p class="card-text">{{$contacto['nombre']}}</p>
            <Strong>Correo:</Strong>
            <p class="card-text">{{$contacto['correo']}}</p>
            <Strong>Mensaje:</Strong>
            <p class="card-text">{{$contacto['mensaje']}}</p>
            
        </div>
   
    </div>
</body>
</html>