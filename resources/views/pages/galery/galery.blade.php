@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Galeria</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Registrar Imagen</h4>
                    </div>
                    @if(session('flash'))
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aviso</strong> {{session('flash')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <form class="col-md-12" action="{{ route('Galeria.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Nombre de la imagen') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="nombre" class="form-control" placeholder="Nombre de la imagen" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Descripcion de la imagen') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="descripcion" class="form-control" placeholder="Descripcion de la imagen" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Foto ') }}</label>
                                    <div class="col-md-9">
                                        <div >
                                            <input type="file" name="foto" class="form-control" placeholder="{{ __('Foto') }}" ................................................................................................................................................................>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn  btn-round">{{ __('Guardar') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> Galeria</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Imagen
                                    </th>
                                    <th>
                                        Descripcion
                                    </th>
                                    <th>
                                        Acciones
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($galeria as $gale=>$gal)
                                        <tr>
                                            <td>{{++$gale}}</td>
                                            <td>
                                                {{ $gal->nombre }}
                                            </td>
                                            <td>
                                                <img class="avatar border-gray" src="{{ asset('storage/'. $gal->foto) }}" alt="...">

                                            </td>
                                            <td>
                                                {{ $gal->descripcion }}
                                            </td>
                                            <td>
                                                <form action="{{route("Galeria.destroy", $gal->id)}}" method="POST"> 
                                                    @method("DELETE")
                                                    @csrf
                                                    <a class="btn btn-primary" href="{{ route('Galeria.edit', $gal->id ) }}">Editar</a>
                                                <button class="btn btn-secondary" type="submit">Eliminar</button>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection