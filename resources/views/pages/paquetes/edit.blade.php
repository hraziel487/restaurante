@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Paquetes</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Editar un Paquete</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form class="col-md-12" action="{{ route('Paquetes.update',$paquetes->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Nombre del paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="nombre" class="form-control" placeholder="Nombre del paquete" value="{{$paquetes->nombre}}" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Descripcion del paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="descripcion" class="form-control" placeholder="Descripcion del paquete" value="{{$paquetes->descripcion}}" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Precio del Paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="number" name="precio" class="form-control" placeholder="Precio del Paquete" value="{{$paquetes->precio}}" required>
                                        </div>
                                        @if ($errors->has('descripcion'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('descripcion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn  btn-round">{{ __('Guardar') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection