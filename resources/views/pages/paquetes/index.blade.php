@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Paquetes</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Registrar un Paquete</h4>
                    </div>
                    @if(session('flash'))
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Aviso</strong> {{session('flash')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <form id="registro-paquete" class="col-md-12">
                                @csrf
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Nombre del paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" id="Nombre" name="Nombre" class="form-control" placeholder="Nombre del paquete" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Descripcion del paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" id="Descripcion" name="Descripcion" class="form-control" placeholder="Descripcion del paquete" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Precio del Paquete') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="number" id="Precio" name="Precio" class="form-control" placeholder="Precio del Paquete" required>
                                        </div>
                                        @if ($errors->has('descripcion'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('descripcion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn  btn-round">{{ __('Guardar') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Listado de Paquetes</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="tabla-paquete">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Descripcion
                                    </th>
                                    <th>
                                        Precio
                                    </th>
                                    <th>
                                        Acciones
                                    </th>
                                    <th>
                                        
                                    </th>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
