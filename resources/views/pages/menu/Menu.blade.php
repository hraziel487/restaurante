@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Menu</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Registrar un Platillo</h4>
                    </div>
                    @if(session('flash'))
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Aviso</strong> {{session('flash')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <form class="col-md-12" action="{{ route('Menu.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Nombre del platillo') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="Nombre" class="form-control" placeholder="Nombre del platillo" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Descripcion del platillo') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="Descripcion" class="form-control" placeholder="Descripcion del platillo" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Precio del Platillo') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="number" name="Precio" class="form-control" placeholder="Precio del Platillo" required>
                                        </div>
                                        @if ($errors->has('descripcion'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('descripcion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn  btn-round">{{ __('Guardar') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Menu</h4>
                    </div>
                     <div class="d-block">
              <!--Filtro Nombre--->
                  <form action="/nombres" method="GET" class="form-group  col-md-5" style="display: inline-block;" >
                  <div style="display:flex; align-items: baseline;">     
                    <label style="margin-right: 0.2rem;">Nombre:</label>     
                    <input type="text" name="nombres" class="form-control" placeholder="Nombre del platillo" required>
                    <input type="submit"  class="btn btn-default" style="display: inline-block;" value="filtrar">
                    </div>
                </form>
                <!-- Fin de filtro -->
            
            <!--Filtro por año--->
                 <form action="/precios" method="GET" class="form-group  col-md-5" style="display: inline-block;" >
                  <div style="display:flex; align-items: baseline;">     
                    <label style="margin-right: 0.2rem;">Precio:</label>     
                    <input type="number" min="0"  name="precios" class="form-control" placeholder="Precio del platillo" required>

                    <input type="submit"  class="btn btn-default" style="display: inline-block;" value="filtrar">
                    </div>
                </form>
    <!-- Fin de filtro -->
    </div>
                  </div>  
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Nombre
                                    </th>
                                    <th>
                                        Descripcion
                                    </th>
                                    <th>
                                        Precio
                                    </th>
                                    <th>
                                        Acciones
                                    </th>
                                    <th>
                                        
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($menu as $plato=>$platillo)
                                        <tr>
                                            <td>{{++$plato}}</td>
                                            <td>
                                                {{ $platillo->nombre }}
                                            </td>
                                             <td>
                                                {{ $platillo->descripcion }}
                                            </td>
                                            <td>
                                                {{ $platillo->precio }}
                                            </td>
                                            <td>
                                                <form action="{{route("Menu.destroy", $platillo->id)}}" method="POST"> 
                                                    @method("DELETE")
                                                    @csrf
                                                    <a class="btn btn-primary" href="{{ route('Menu.edit', $platillo->id ) }}">Editar</a>
                                                <button class="btn btn-secondary" type="submit">Eliminar</button>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
