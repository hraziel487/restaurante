@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'map'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Reportes</h5>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Registrar un Reporte</h4>
                    </div>
                    @if(session('flash'))
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Aviso</strong> {{session('flash')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <form class="col-md-12" action="{{ route('Reportes.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Titulo del Reporte') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="Titulo" class="form-control" placeholder="Titulo del Reporte" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Proposito del Reporte') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="Proposito" class="form-control" placeholder="Proposito del Reporte" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Descripcion del Reporte') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="Descripcion" class="form-control" placeholder="Descripcion del Reporte" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <button type="submit" class="btn  btn-round">{{ __('Guardar') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Lista de Reportes</h4>
                    </div>
                  </div>  
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Titulo
                                    </th>
                                    <th>
                                        Proposito
                                    </th>
                                    <th>
                                        Descripcion
                                    </th>
                                    <th>
                                        Acciones
                                    </th>
                                    <th>
                                        
                                    </th>
                                </thead>
                                <tbody>
                                    @foreach ($reporte as $report=>$repor)
                                        <tr>
                                            <td>{{++$report}}</td>
                                            <td>
                                                {{ $repor->Titulo }}
                                            </td>
                                            <td>
                                                {{ $repor->Proposito }}
                                            </td>
                                            <td>
                                                {{ $repor->Descripcion }}
                                            </td>
                                            
                                            <td>
    
                                                
                                                <form action="{{route("Reportes.destroy", $repor->id)}}" method="POST"> 
                                                    @method("DELETE")
                                                    @csrf
                                                    <a class="btn btn-primary" href="{{ route('Reportes.edit', $repor->id ) }}">Editar</a>
                                                <button class="btn btn-secondary" type="submit">Eliminar</button>
                                                <a class="btn btn-primary" style="" href="{{ route('Reportes.pdf', $repor->id ) }}">Descargar </a>
                                                </form>
                                        


                                </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
