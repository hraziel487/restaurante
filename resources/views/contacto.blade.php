@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

<style>
    label{
        font-family: league-gothic, sans-serif;
        color: white !important;
        font-size: 25px !important;
        
    }

    .card-header{
        font-family: league-gothic, sans-serif;
        font-size: 35px !important;
    }
</style>

@section('content')
    <div class="content col-md-10 ml-auto mr-auto">
        <div class="header py-5 pb-7 pt-lg-9">
            <div class="row">   
                <div class="col-sm-6" style="border-left-width: 3px !important; border-left-color:#e4c27b !important; border-left-style: solid !important; transition: width 2s !important;">
                    <div class="card" style="background: #000000 !important; ">
                        <h5 class="card-header" style="color:#e4c27b !important">Contacto</h5>
                        @if(session('flash'))
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Aviso</strong> {{session('flash')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        @endif
                        <div class="card-body">
                            <form action="{{route('Contacto.store')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Nombre Completo</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Ejemplo: Rudy Hernandez" name="nombre" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Correo Electronico</label>
                                    <input type="email" class="form-control" id="inlineFormInputName" placeholder="Ejemplo@gmail.com" name="correo" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Comentario</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Escriba su comentario" name="mensaje" required></textarea>
                                </div>
                                <div class="d-flex justify-content-center mt-4" >
                                    <button type="submit" class="btn btn-warning">Enviar Comentario</button>
                                </div>
                            </form>
                        </div>
                    </div>       
                </div>

                <div class="col-xs-12 col-sm-6 col-md-5 mx-auto" style="background: #000000; box-shadow: 2px 2px 13px RGB(242, 243, 244) !important;"">
                    <div class="card-body">
                        <div class="d-flex justify-content-center mt-5" >
                            <img src="{{ asset('paper/img/logo.svg') }}" class="rounded-circle" alt="Eniun" ">
                        </div>
                            <br>
                        <h5 class="text-center white" style="color: aliceblue;">998 204 5738</h5>
                            <br>
                        <h5 class="text-center white" style="color: aliceblue;">Av. Kabah Región 228 Mza 104 Lote 17 77516 Cancún, México</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection