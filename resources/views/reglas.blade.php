
@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

@section('content')
<div class="content col-md-12 ml-auto mr-auto">
    <div class="header py-5 pb-7 pt-lg-9">
        <div class="container col-md-10">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12 ">
                        <h4 class="titulo" style="font-size: 90px !important">{{ __('REGLAS DEL RESTURANTE') }}</h4>
                            <div class="container text-justify mt-5" style="border-left-width: 3px !important; border-left-color:#e4c27b !important; border-left-style: solid !important; color:white">
                                @foreach ($regla as $rule=>$reg)
                                <p class="reglatext">{{++$rule}}.- {{ $reg->Descripcion }}</p>
                                @endforeach             
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
