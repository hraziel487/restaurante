@extends('errors::illustrated-layout')

@section('code', '404')

@section('title', __('Página no encontrada'))

@section('image')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<style>
    #apartado-derecho{
        text-align:center;
    }
    ul{
        text-decoration: none !important;
        list-style: none;
        color: black;
        font-weight: bold;
    }
</style>

<div id="apartado-derecho" style="background-color: #000000;margin-top: -20px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">

<div>
<h1>
<img src="{{ asset('paper/img/logo.svg') }}" class="rounded-circle" alt="Eniun"  alt="" class="imageninventario" width="550" height="550">
</h1>
<div class="copyright ml-auto text-center"  style="color: white">
		<br>	
    				<b>Copyright</b> BRAHMAN  @ CORTES AHUMADOS
          
						</div>	


</div>


</div>
@endsection

@section('message', __('"Usted no esta autorizado para ver esta informacion"'))

