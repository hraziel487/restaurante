@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

@section('content')
<div class="content col-md-12 ml-auto mr-auto">
    <div class="header py-5 pb-7 pt-lg-9">
        <div class="container col-md-10">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12 ">
                        <h1 class="titulo">{{ __('PAQUETES') }}</h1>

                            <div class="container">
                                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
                                   <!-- <div id="linea-vertical"></div> -->
                                  <div class="col subtitulo  horizontal">                                   
                                    <button type="button" class="btn btn-primary" style="background: transparent !important;" data-toggle="modal" data-target="#exampleModal">
                                        BRAHM <br>BQ  
                                      </button>
                                    <div class="one"></div>
                                    <div class="two"></div>
                                    <div class="three"></div>
                                  </div>

                                    <div class="col bloque">
                                        <button type="button" class="btn btn-primary" style="background: transparent !important;" data-toggle="modal" data-target="#exampleModal-2">
                                            BHAHMAN INDIVIDUAL
                                          </button>
                                    <div class="one"></div>
                                    <div class="two"></div>
                                    <div class="three"></div>
                                    </div>
                                    <div class="col bloque  horizontal">
                                        <button type="button" class="btn btn-primary" style="background: transparent !important;" data-toggle="modal" data-target="#exampleModal-3">
                                            PAREJA BRAHMAN
                                          </button>
                                        <div class="one"></div>
                                        <div class="two"></div>
                                        <div class="three"></div> 

                                    </div>
                                    <div class="col subtitulo" style="font-size: 51px;">
                                        <button type="button" class="btn btn-primary" style="background: transparent !important;" data-toggle="modal" data-target="#exampleModal-4">
                                            STEAK <br>HOUSE
                                          </button>
                                        <div class="one"></div>
                                        <div class="two"></div>
                                        <div class="three"></div></div>
                                    </div>
                                   <!-- <hr id="linea"> -->
                                </div>
                                <h1 class="subtitulo mt-5"><button type="button" class="btn btn-primary" style="background: transparent !important;" data-toggle="modal" data-target="#exampleModal-5">
                                    FAMILIA BRAHMAN
                                  </button></h1>
                              </div>
                                                  
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="background: black;">
        <div class="modal-header">
            @foreach ($paquetes as $paquete=>$pack)  @if($loop->index == 0)
          <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;color: #e4c27b;font-weight: bolder;">{{ $pack->nombre }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div class="modal-body" style="color: white;text-align: center;">
             {{ $pack->descripcion }}   <span style="color: #e4c27b; font-weight: bolder;">${{ $pack->precio }}.00</span> @endif @endforeach
        </div>
        <div> <img class="img-responsive" src="{{ asset('paper/img/platillos/branmar resta (3).jpg') }}" > 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" style="margin: auto;" data-dismiss="modal">Close</button>

        </div>
      </div>
    </div>
  </div>
    <!-- Modal 2-->
    <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style="background: black;">
            <div class="modal-header">
                @foreach ($paquetes as $paquete=>$pack)  @if($loop->index == 1)
              <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;color: #e4c27b;font-weight: bolder;">{{ $pack->nombre }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div class="modal-body" style="color: white;text-align: center;">
                 {{ $pack->descripcion }}   <span style="color: #e4c27b; font-weight: bolder;">${{ $pack->precio }}.00</span> @endif @endforeach
            </div>
            <div> <img class="img-responsive" src="{{ asset('paper/img/platillos/IMG-3812 (2).jpg') }}" > 

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" style="margin: auto;" data-dismiss="modal">Close</button>
    
            </div>
          </div>
        </div>
      </div>
       <!-- Modal 3-->
    <div class="modal fade" id="exampleModal-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style="background: black;">
            <div class="modal-header">
                @foreach ($paquetes as $paquete=>$pack)  @if($loop->index == 2)
              <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;color: #e4c27b;font-weight: bolder;">{{ $pack->nombre }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div class="modal-body" style="color: white;text-align: center;">
                 {{ $pack->descripcion }}  <br> <span style="color: #e4c27b; font-weight: bolder;">${{ $pack->precio }}.00</span> @endif @endforeach
            </div>
            <div> <img class="img-responsive" src="{{ asset('paper/img/platillos/WhatsApp Image 2020-12-04 at 4.59.27 PM (2).jpeg') }}" > 

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" style="margin: auto;" data-dismiss="modal">Close</button>
    
            </div>
          </div>
        </div>
      </div>
             <!-- Modal 4-->
    <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style="background: black;">
            <div class="modal-header">
                @foreach ($paquetes as $paquete=>$pack)  @if($loop->index == 3)
              <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;color: #e4c27b;font-weight: bolder;">{{ $pack->nombre }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div class="modal-body" style="color: white;text-align: center;">
                 {{ $pack->descripcion }}  <br> <span style="color: #e4c27b; font-weight: bolder;">${{ $pack->precio }}.00</span> @endif @endforeach
            </div>
            <div> <img class="img-responsive" src="{{ asset('paper/img/platillos/WhatsApp Image 2020-12-08 at 9.49.40 PM (2).jpeg') }}" > 

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" style="margin: auto;" data-dismiss="modal">Close</button>
    
            </div>
          </div>
        </div>
      </div>
                   <!-- Modal 5-->
    <div class="modal fade" id="exampleModal-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content" style="background: black;">
            <div class="modal-header">
                @foreach ($paquetes as $paquete=>$pack)  @if($loop->index == 4)
              <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;color: #e4c27b;font-weight: bolder;">{{ $pack->nombre }}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"></span>
              </button>
            </div>
            <div class="modal-body" style="color: white;text-align: center;">
                 {{ $pack->descripcion }}  <br> <span style="color: #e4c27b; font-weight: bolder;">${{ $pack->precio }}.00</span> @endif @endforeach
            </div>
            <div> <img class="img-responsive" src="{{ asset('paper/img/platillos/WhatsApp Image 2020-12-08 at 10.21.39 PM (3).jpeg') }}" > 

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" style="margin: auto;" data-dismiss="modal">Close</button>
    
            </div>
          </div>
        </div>
      </div>
@endsection
