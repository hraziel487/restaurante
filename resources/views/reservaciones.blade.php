@extends('layouts.app', [
    'class' => 'login-page',
    'elementActive' => ''
])

@section('content')
<div class="content col-md-12 ml-auto mr-auto">
    <div class="header py-5 pb-7 pt-lg-9">
        <div class="container col-md-10">
            <center><h5 class="titulo">{{ __('RESERVACIONES') }}</h5></center>
            <div id="wrapper">
            <main class="sidebar-open">
                <div class="column">
                  <h4>Lunes</h4>
                  <div class="column__content">
                    <ul class="list-group">
                      <li class="list-group-item">12:00</li>
                      <li class="list-group-item">13:00</li>
                      <li class="list-group-item">14:00</li>
                      <li class="list-group-item">15:00</li>
                      <li class="list-group-item">16:00</li>
                      <li class="list-group-item">17:00</li>
                      <li class="list-group-item">18:00</li>
                      <li class="list-group-item">19:00</li>
                      <li class="list-group-item">20:00</li>
                      <li class="list-group-item">21:00</li>
                      <li class="list-group-item">22:00</li>
                    </ul>
                    
                  </div>
                </div>
                <div class="column">
                  <h4>Martes</h4>
                  <div class="column__content">
                    <ul class="list-group">
                      <li class="list-group-item">12:00</li>
                      <li class="list-group-item">13:00</li>
                      <li class="list-group-item">14:00</li>
                      <li class="list-group-item">15:00</li>
                      <li class="list-group-item">16:00</li>
                      <li class="list-group-item">17:00</li>
                      <li class="list-group-item">18:00</li>
                      <li class="list-group-item">19:00</li>
                      <li class="list-group-item">20:00</li>
                      <li class="list-group-item">21:00</li>
                      <li class="list-group-item">22:00</li>
                    </ul>
                  </div>
                </div>
                <div class="column">
                  <h4>Miercoles</h4>
                  <div class="column__content">
                    <ul class="list-group">
                      <li class="list-group-item">12:00</li>
                      <li class="list-group-item">13:00</li>
                      <li class="list-group-item">14:00</li>
                      <li class="list-group-item">15:00</li>
                      <li class="list-group-item">16:00</li>
                      <li class="list-group-item">17:00</li>
                      <li class="list-group-item">18:00</li>
                      <li class="list-group-item">19:00</li>
                      <li class="list-group-item">20:00</li>
                      <li class="list-group-item">21:00</li>
                      <li class="list-group-item">22:00</li>
                    </ul>  
                  </div>
                </div>
                <div class="column">
                  <h4>Jueves</h4>
                  <div class="column__content">
                    <ul class="list-group">
                      <li class="list-group-item">12:00</li>
                      <li class="list-group-item">13:00</li>
                      <li class="list-group-item">14:00</li>
                      <li class="list-group-item">15:00</li>
                      <li class="list-group-item">16:00</li>
                      <li class="list-group-item">17:00</li>
                      <li class="list-group-item">18:00</li>
                      <li class="list-group-item">19:00</li>
                      <li class="list-group-item">20:00</li>
                      <li class="list-group-item">21:00</li>
                      <li class="list-group-item">22:00</li>
                    </ul>
                 </div>
                </div>
                <div class="column">
                    <h4>Viernes</h4>
                    <div class="column__content">
                      <ul class="list-group">
                        <li class="list-group-item">12:00</li>
                        <li class="list-group-item">13:00</li>
                        <li class="list-group-item">14:00</li>
                        <li class="list-group-item">15:00</li>
                        <li class="list-group-item">16:00</li>
                        <li class="list-group-item">17:00</li>
                        <li class="list-group-item">18:00</li>
                        <li class="list-group-item">19:00</li>
                        <li class="list-group-item">20:00</li>
                        <li class="list-group-item">21:00</li>
                        <li class="list-group-item">22:00</li>
                      </ul>
                        </div>
                      </div>

                  </main>
                </div>
                <div id="reserva-braham">
                  <div class="horario mt-4">
                  <div class="scrollmenu">
                    <p  id="sabado" >Sabado</p>
                    <a href="#">12:00</a>
                    <a href="#">13:00</a>
                    <a href="#">14:00</a>
                    <a href="#">15:00</a>
                    <a href="#">16:00</a>
                    <a href="#">17:00</a>
                    <a href="#">18:00</a>  
                    <a href="#">19:00</a>
                    <a href="#">20:00</a>
                    <a href="#">21:00</a>
                    <a href="#">22:00</a>
                  </div>
                </div>
                   <img src="{{ asset('paper/img/logo.svg') }}" id="logo-reservaciones" > 
                  <div class="horario">
                  <div class="scrollmenu">
                    <p  id="domingo">Domingo</p>
                    <a href="#">12:00</a>
                    <a href="#">13:00</a>
                    <a href="#">14:00</a>
                    <a href="#">15:00</a>
                    <a href="#">16:00</a>
                    <a href="#">17:00</a>
                    <a href="#">18:00</a>  
                    <a href="#">19:00</a>
                    <a href="#">20:00</a>
                    <a href="#">21:00</a>
                    <a href="#">22:00</a>
                  </div>
                </div>
                </div> 
            </div>
        </div>
    </div>
  </div> 
</div>
@endsection