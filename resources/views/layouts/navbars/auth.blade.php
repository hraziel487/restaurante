<div class="sidebar" data-color="black" data-active-color="warning">
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo.svg">
            </div>
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            {{ __('Panel Brahman') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'dashboard') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            @role('Usuario')
            <li class="{{ $elementActive == 'Reglas' ? 'active' : '' }}">
                <a href="{{ route('Reglas.index') }}">
                    <i class="nc-icon nc-diamond"></i>
                    <p>{{ __('Reglas') }}</p>
                </a>
            </li>
            @endrole
            @role('Usuario')
            <li class="{{ $elementActive == 'galery' ? 'active' : '' }}">
                <a href="{{ route('Galeria.index') }}">
                    <i class="nc-icon nc-image"></i>
                    <p>{{ __('Galeria') }}</p>
                </a>
            </li>
            @endrole
                @role('Administrador')
            <li class="{{ $elementActive == 'notifications' ? 'active' : '' }}">
                <a href="{{ route('Reportes.index') }}">
                    <i class="nc-icon nc-bell-55"></i>
                    <p>{{ __('Reportes') }}</p>
                </a>
            </li>
            @endrole
            @role('Administrador')
            <li class="{{ $elementActive == 'Menu' ? 'active' : '' }}">
                <a href="{{ route('Menu.index') }}">
                    <i class="nc-icon nc-tile-56"></i>
                    <p>{{ __('Menu') }}</p>
                </a>
            </li>
            @endrole
            @role('Administrador')
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('Paquetes.index') }}">
                    <i class="nc-icon nc-bag-16"></i>
                    <p>{{ __('Paquetes') }}</p>
                </a>
            </li>
            @endrole
            @role('Administrador')
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('Descuentos.index') }}">
                    <i class="nc-icon nc-sound-wave"></i>
                    <p>{{ __('Descuentos') }}</p>
                </a>
            </li>
            @endrole
            @role('Administradr')
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('Paquetes_has_Descuentos.index') }}">
                    <i class="nc-icon nc-layout-11"></i>
                    <p>{{ __('Control de descuentos') }}</p>
                </a>
            </li>
            @endrole
        </ul>
    </div>
</div>
