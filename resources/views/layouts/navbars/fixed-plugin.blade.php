<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>
        <ul class="dropdown-menu">
            <li class="header-title"> Sidebar Background</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger background-color">
                    <div class="badge-colors text-center">
                        <span class="badge filter badge-light active" data-color="white"></span>
                        <span class="badge filter badge-dark" data-color="black"></span>
                        <span class="badge filter badge-dark" data-color="RED"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
           
            
        </ul>
    </div>
</div>