<style>
    .nav-link:after{
        content: '';
        display: block;
        width: 0;
        height: 3px;
        background: #E4C27B;
        transition: width .3s;



    }

    .nav-link:hover::after{
        width: 100%;
        
    
    }

</style>


<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
    <div class="container">
        <img src="{{ asset('paper/img/logo.svg') }}" id="logo" >

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span style="background:  #E4C27B !important;" class="navbar-toggler-bar bar1"></span>
            <span style="background:  #E4C27B !important;" class="navbar-toggler-bar bar2"></span>
            <span style="background:  #E4C27B !important;"class="navbar-toggler-bar bar3"></span>
        </button>
        
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">
                    {{ __('HOME') }}
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{route('Paquetes')}}" class="nav-link">
                    {{ __('PAQUETES') }}
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{route('Menu')}}" class="nav-link">
                    {{ __('MENÚ') }}
                    </a>
                </li>
                <!--
                <li class="nav-item  active ">
                    <a href="{{ route('Reservaciones') }}" class="nav-link">
                    {{ __('RESERVACIONES') }}
                    </a>
                </li>
                   -->                
                <li class="nav-item  active ">
                    <a href="{{route('Galeria')}}" class="nav-link">
                    {{ __('GALERÍA') }}
                    </a>
                </li>
                <li class="nav-item  active ">
                    <a href="{{ route('Reglas') }}" class="nav-link">
                    {{ __('REGLAMENTO') }}
                    </a>
                </li>
                <li class="nav-item  active ">
                    <a href="{{route('Contacto')}}" class="nav-link">
                    {{ __('CONTACTO') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>