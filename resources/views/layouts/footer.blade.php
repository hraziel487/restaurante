<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li>
                        <a href="#" target="_blank"><img src="{{ asset('paper/img/facebook.png') }}"></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><img src="{{ asset('paper/img/whats.png') }}"></a>
                    </li>
                    <li>
                        <a href="#" target="_blank"><img src="{{ asset('paper/img/insta.png') }}"></a>
                    </li>
                </ul>
            </nav>
            <div class="col-lg-9 text-center">
                <span style="color: aliceblue;">
                    BRAHMAN CORTES AHUMADOS
                </span>
            </div>
        </div>
    </div>

</footer>