<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaquetesHasDescuentos extends Model
{
    use HasFactory;
    protected $fillable = [
        'paquetes_id',
        'descuentos_id'    
    ];
}
