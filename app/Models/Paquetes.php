<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquetes extends Model
{
    use HasFactory;

    protected $table = "Paquetes";

    protected $fillable = [
        'nombre',
        'descripcion',
        'precio'
    ];

    public $timestamps = false;
}
