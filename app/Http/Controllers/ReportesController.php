<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reporte;
use PDF;

class ReportesController extends Controller
{
    public function index()
    {
        $reporte = Reporte::all();
        return view('pages.reportes.index', compact('reporte'));
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {  
        
            $reporte = new Reporte;
            $reporte->Titulo = $request->Titulo;
            $reporte->Proposito = $request->Proposito;
            $reporte->Descripcion = $request->Descripcion;
//            $reporte->documento = $request->documento;
            $reporte->save();    
            $reporte = Reporte::all();
            return redirect()->route('Reportes.index', compact('reporte'))->with('flash','Reporte guardado satisfactoriamente.');
    }

    public function show(Reporte $reporte)
    {
        //
    }

    public function edit($id)
    {
        $reporte = Reporte::findOrFail($id);
        return view('pages.reportes.edit', compact('reporte'));
    }

    public function update(Request $request, $id)
    {
        $reporte = Reporte::findOrFail($id);
        $reporte->fill($request->all());
        $reporte->save();
        return redirect()->route('Reportes.index')->with('flash','Reporte actualizado Satisfactoriamente.');
    }

    public function destroy($id)
    {
        $data = Reporte::findOrFail($id);
        $data->delete();
        return redirect()->route('Reportes.index')->with('flash','Reporte eliminado Satisfactoriamente.');
    }

    public function pdf($id){
        $reporte = Reporte::find($id);
        $pdf = PDF::loadView('pages.reportes.pdf',$reporte);
        return $pdf->download('Reportes.pdf');
      }

}
