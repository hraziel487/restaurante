<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Regla;

class reglasController extends Controller
{
    public function index()
    {
        $regla = Regla::all();
        return view('pages.reglas.index', compact('regla'));
    }

    public function Reglas()
    {
        $regla = Regla::all();
        return view('Reglas', compact('regla'));
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {  
        
            $regla = new Regla;
            $regla->Descripcion = $request->Descripcion;
            $regla->save();    
            $regla = Regla::all();
            return redirect()->route('Reglas.index', compact('regla'))->with('flash','Regla agregado Satisfactoriamente.');
    }

    public function show(Regla $regla)
    {
        //
    }

    public function edit($id)
    {
        $regla = Regla::findOrFail($id);
        return view('pages.reglas.edit', compact('regla'));
    }

    public function update(Request $request, $id)
    {
        $regla = Regla::findOrFail($id);
        $regla->fill($request->all());
        $regla->save();
        return redirect()->route('Reglas.index')->with('flash','Regla actualizado Satisfactoriamente. ');
    }

    public function destroy($id)
    {
        $data = Regla::findOrFail($id);
        $data->delete();
        return redirect()->route('Reglas.index')->with('flash','Regla eliminado Satisfactoriamente.');
    }
}
