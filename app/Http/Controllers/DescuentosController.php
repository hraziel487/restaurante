<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Descuentos;

class DescuentosController extends Controller
{
    public function index()
    {
        $descuentos = Descuentos::all();
        return view('pages.descuentos.index', compact('descuentos'));
    }
    public function Descuentos()
    {
        $descuentos = Descuentos::all();
        return view('descuentos', compact('descuentos'));
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {   
            $descuentos = new Descuentos;
            $descuentos->cantDesc = $request->cantDesc;
            $descuentos->descripcion = $request->descripcion;
            $descuentos->save();    
            $descuentos = Descuentos::all();
            return redirect()->route('Descuentos.index', compact('descuentos'))->with('flash','Descuento agregado Satisfactoriamente.');
    }

    public function show(Descuentos $descuentos)
    {
        //
    }

    public function edit($id)
    {
        $descuentos = Descuentos::findOrFail($id);
        return view('pages.descuentos.edit', compact('descuentos'));
    }

    public function update(Request $request, $id)
    {
        $descuentos = Descuentos::findOrFail($id);
        $descuentos->fill($request->all());
        $descuentos->save();
        return redirect()->route('Descuentos.index')->with('flash','Descuento actualizado Satisfactoriamente.');
    }

    public function destroy($id)
    {
        $data = Descuentos::findOrFail($id);
        $data->delete();
        return redirect()->route('Descuentos.index')->with('flash','Descuento eliminado Satisfactoriamente.');

    }
}
