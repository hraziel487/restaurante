<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paquetes;
use Illuminate\Support\Facades\DB;
use DataTables;

class PaquetesController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $paquetes = DB::select('CALL spsel_paquete()');

            return DataTables::of($paquetes)
                ->addColumn('acciones', function($paquetes){
                    $acciones = '<a href="" class="btn btn-info btn-sm">Editar</a>';
                    $acciones .= '<button type="button" name="delete" id="" class="btn btn-danger btn-sm">Eliminar</button>';
                    return $acciones;
                })
                ->rawColumns(['acciones'])
                ->make(true);
        }

        // $paquetes = Paquetes::all();
        return view('pages.paquetes.index');
    }
    public function Paquetes()
    {
        $paquetes = Paquetes::all();
        return view('paquetes', compact('paquetes'));
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {
        $paquete = DB::statement('CALL spcre_paquetes(?,?,?)', 
            [$request->nombre, $request->descripcion, $request->precio]);
        
        return back();
    }

    public function show(Paquetes $paquetes)
    {
        //
    }

    public function edit($id)
    {
        $paquetes = Paquetes::findOrFail($id);
        return view('pages.paquetes.edit', compact('paquetes'));
    }

    public function update(Request $request, $id)
    {
        $paquetes = Paquetes::findOrFail($id);
        $paquetes->fill($request->all());
        $paquetes->save();
        return redirect()->route('Paquetes.index')->with('flash','Paquete actualizado Satisfactoriamente.');
    }

    public function destroy($id)
    {
        $data = Paquetes::findOrFail($id);
        $data->delete();
        return redirect()->route('Paquetes.index')->with('flash','Paquete eliminado Satisfactoriamente.');

    }
}
