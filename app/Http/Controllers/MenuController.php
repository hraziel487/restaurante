<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Platillos;
use Response;

class MenuController extends Controller
{
    public function index()
    {
        $menu = Platillos::all();
        return view('Menu', compact('menu'));
    }
    public function Menu()
    {
        $menu = Platillos::all();

        return view('pages.menu.Menu', compact('menu'));
    }

    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {  
        
            $menu = new Platillos;
            $menu->nombre = $request->Nombre;
            $menu->descripcion = $request->Descripcion;
            $menu->precio = $request->Precio;
            $menu->save();    
            $menu = Platillos::all();
       // return Response::json($menu);
        return redirect()->route('Menu.index', compact('menu'))->with('flash','Menu creado Satisfactoriamente.');
        /*
        $menu = new Platillos();
        $menu->nombre = $request->Nombre;
        $menu->descripcion = $request->Descripcion;
        $menu->precio = $request->Precio;
        $menu->save();    
        $menu = Platillos::all();  
        return response()->json($menu);
        */
    }

    public function show(Platillos $menu)
    {
        //
    }

    public function edit($id)
    {
        $menu = Platillos::findOrFail($id);
        return view('pages.menu.edit', compact('menu'));
    }

    public function update(Request $request, $id)
    {
        $menu = Platillos::findOrFail($id);
        $menu->fill($request->all());
        $menu->save();
        return redirect()->route('Menu.index')->with('flash','Platillo actualizado Satisfactoriamente.');
        //return Response::json($menu);
    }

    public function destroy($id)
    {
        $data = Platillos::findOrFail($id);
        $data->delete();
        return redirect()->route('Menu.index')->with('flash','Platillo eliminado Satisfactoriamente.');
        // return Response::json($menu);
    }
    public function nombres(Request $request){
        $nombres = $request->get('nombres');      
        $menu=Platillos::where('nombre','like',"%$nombres%")->get();
        return view('pages.menu.Menu',compact('menu'));
    }
    public function precios(Request $request){
        $precios = $request->get('precios');      
        $menu=Platillos::where('precio','like',"%$precios%")->get();
        return view('pages.menu.Menu',compact('menu'));
    }
}
