<?php

namespace App\Http\Controllers;

use App\Mail\ContactoMail;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
    public function index()
    {
        return view('contacto');
    }

    public function store(Request $Request)
    {
        $correo = new ContactoMail($Request->all());
        Mail::to('nomixios12@gmail.com')->send($correo);

        return redirect()->route('Contacto')->with('flash','Mensaje enviado.');
    }
}

