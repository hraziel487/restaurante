<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Galeria;

class GaleriaController extends Controller
{

    public function index()
    {
        $galeria = Galeria::all();
        return view('Galeria', compact('galeria'));
    }
    public function Galeria()
    {
        $galeria = Galeria::all();
        return view('galeria', compact('galeria'));
    }
    
    public function store(Request $request){
        
        $galeria = new Galeria;
        $galeria->nombre = $request->nombre;
        if($request->file('foto'))
        {
          $file=$request->file('foto');
          $filename='foto'.$request->nombre.'.'.$file->getClientOriginalExtension();
          $request->foto->move('storage/' , $filename);
          $galeria->foto=$filename;
        }
        $galeria->foto = $filename;
        $galeria->descripcion = $request->descripcion;
        $galeria->save();

        $galeria = Galeria::all();

        return view('pages.galery.galery', compact('galeria'))->with('flash','Imagen agregado Satisfactoriamente.');
    }

    public function show(Galeria $galeria)
    {
        //
    }

    public function edit($id)
    {
        $galeria = Galeria::findOrFail($id);
        return view('pages.galery.edit', compact('galeria'));
    }

    public function update(Request $request, $id)
    {
        $galeria = Galeria::findOrFail($id);
        $galeria->fill($request->all());
        if($request->hasfile('foto')){
            $file=$request->file('foto');
            $filename='foto'.$request->nombre.'.'.$file->getClientOriginalExtension();
            $request->foto->move('storage/' , $filename);
            $galeria->foto=$filename;
         
        }
        $galeria->save();
        return redirect()->route('Galeria.index')->with('flash','Imagen actualizado Satisfactoriamente.');
    }
    public function destroy($id)
    {
        $data = Galeria::findOrFail($id);
        $data->delete();
        return redirect()->route('Galeria.index')->with('flash','Imagen eliminado Satisfactoriamente.');
    }

    public function galery(){
        $galeria = Galeria::all();
        return view('pages.galery.galery', compact('galeria'));
    }
}
