<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Descuentos;
use App\Models\Paquetes;
use App\Models\PaquetesHasDescuentos;
use Illuminate\Support\Facades\DB;

class Paquetes_has_DescuentosController extends Controller
{
    public function index(PaquetesHasDescuentos $model)
    {
        $paquetes = Paquetes::all();
        $descuentos = PaquetesHasDescuentos::all();
       
        $paquetes = DB::table('paquetes')
            ->select('paquetes.id as id', 'paquetes.nombre as nombre', 'paquetes.descripcion as descripcion', 'paquetes.precio as precio', 'descuentos.cantDesc as cantDesc', 'descuentos.descripcion as description')
            ->join('paquetes_has_descuentos', 'paquetes_has_descuentos.paquetes_id', '=', 'paquetes.id')
            ->join('descuentos', 'paquetes_has_descuentos.paquetes_id', '=', 'descuentos.id')
            ->paginate(5);
         

        return view('pages.paquetes_has_descuentos.index', compact('paquetes','descuentos'));
    }
/*
    public function Descuentos()
    {
        $descuentos = Descuentos::all();
        return view('descuentos', compact('descuentos'));
    }
*/
    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {  
            $busqueda = PaquetesDescuentos::where('paquetes_id', $request->paquetes)->where('descuentos_id', $request->descuentos)->first();        
            if(!isset($busqueda))
            {
            $PaqDesc = new PaquetesDescuentos;
            $PaqDesc->paquetes_id = $request->paquetes;
            $PaqDesc->descuentos_id = $request->descuentos;
            $PaqDesc->save();    
            $PaqDesc = PaquetesDescuentos::all();
                return view('pages.paquetes_has_descuentos.index', compact('PaqDesc'));
            }
            else
            {
                return view('pages.paquetes_has_descuentos.index', compact('PaqDesc'));            }
       
    }

    public function show(PaquetesDescuentos $descuentos)
    {
        //
    }

    public function edit($id)
    {
        $paquetes = Paquetes::findOrFail($id);
        $paqDes = PaquetesDescuentos::where('paquetes_id', $id)->first();   
        $a = Descuentos::all();      
        return view('pages.paquetes_has_descuentos.edit', compact('paquetes','paqDes'),['a' => $a]);
    }

    public function update(Request $request, $id)
    {
        $data = PaquetesDescuentos::where('paquetes_id',$id)
        ->update([
            "descuentos_id" => $request->descuentos
        ]);

        return redirect()->route('paquetes_has_descuentos.index')->with('flash','Platillo actualizado Satisfactoriamente.');
    }

    public function destroy(Request $request)
    {
        $data = PaquetesDescuentos::where('paquetes_id', $request->id);
        if(isset($data))
        {
            $data->delete();
            return redirect()->route('paquetes_has_descuentos.index')->with('flash','Platillo eliminado Satisfactoriamente.');
        }
        

    }
}
