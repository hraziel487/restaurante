<?php

use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('/home', 'App\Http\Controllers\HomeController@index');

Route::get('/inicio', 'App\Http\Controllers\UserController@home')->name('home');

//Paquetes
Route::get('/paquetes', 'App\Http\Controllers\PaquetesController@Paquetes')->name('Paquetes');

//Galeria
Route::get('/galeria', 'App\Http\Controllers\GaleriaController@Galeria')->name('Galeria');

//Reservaciones
Route::get('/reservaciones', 'App\Http\Controllers\ReservacionesController@index')->name('Reservaciones');

//Menu
Route::get('/menu', 'App\Http\Controllers\MenuController@index')->name('Menu');

//reglas 
Route::get('/reglas', 'App\Http\Controllers\reglasController@Reglas')->name('Reglas');

//contacto

Route::get('/contacto', 'App\Http\Controllers\ContactoController@index')->name('Contacto');

Route::post('/contacto', 'App\Http\Controllers\ContactoController@store')->name('Contacto.store');




Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => ['role:Administrador']], function () { 

	//Menu

	Route::post('/Menu', 'App\Http\Controllers\MenuController@store')->name('Menu.store');
	Route::get('/Menu', 'App\Http\Controllers\MenuController@Menu')->name('Menu.index');
	Route::get('/nombres','App\Http\Controllers\MenuController@nombres');
	Route::get('/precios','App\Http\Controllers\MenuController@precios');
	Route::get('/Menu/{id}/edit','App\Http\Controllers\MenuController@edit')->name('Menu.edit');
	Route::put('/Menu/{id}','App\Http\Controllers\MenuController@update')->name('Menu.update');
	Route::delete('/Menu/{id}','App\Http\Controllers\MenuController@destroy')->name('Menu.destroy');

	//Reportes
	Route::post('/Reportes', 'App\Http\Controllers\ReportesController@store')->name('Reportes.store');
	Route::get('/Reportes', 'App\Http\Controllers\ReportesController@index')->name('Reportes.index');
	Route::get('/Reportes/{id}/edit','App\Http\Controllers\ReportesController@edit')->name('Reportes.edit');
	Route::put('/Reportes/{id}','App\Http\Controllers\ReportesController@update')->name('Reportes.update');
	Route::delete('/Reportes/{id}','App\Http\Controllers\ReportesController@destroy')->name('Reportes.destroy');
	Route::get('/Reportes/download/{id}','App\Http\Controllers\ReportesController@pdf')->name('Reportes.pdf');


	//Paquetes
	Route::post('/Paquetes', 'App\Http\Controllers\PaquetesController@store')->name('Paquetes.store');
	Route::get('/Paquetes', 'App\Http\Controllers\PaquetesController@index')->name('Paquetes.index');
	Route::get('/Paquetes/{id}/edit','App\Http\Controllers\PaquetesController@edit')->name('Paquetes.edit');
	Route::put('/Paquetes/{id}','App\Http\Controllers\PaquetesController@update')->name('Paquetes.update');
	Route::delete('/Paquetes/{id}','App\Http\Controllers\PaquetesController@destroy')->name('Paquetes.destroy');

	//Descuentos
	Route::post('/Descuentos', 'App\Http\Controllers\DescuentosController@store')->name('Descuentos.store');
	Route::get('/Descuentos', 'App\Http\Controllers\DescuentosController@index')->name('Descuentos.index');
	Route::get('/Descuentos/{id}/edit','App\Http\Controllers\DescuentosController@edit')->name('Descuentos.edit');
	Route::put('/Descuentos/{id}','App\Http\Controllers\DescuentosController@update')->name('Descuentos.update');
	Route::delete('/Descuentos/{id}','App\Http\Controllers\DescuentosController@destroy')->name('Descuentos.destroy');

	//Paquetes_has_Descuentos
	Route::post('/Paquetes_has_Descuentos', 'App\Http\Controllers\Paquetes_has_DescuentosController@store')->name('Paquetes_has_Descuentos.store');
	Route::get('/Paquetes_has_Descuentos', 'App\Http\Controllers\Paquetes_has_DescuentosController@index')->name('Paquetes_has_Descuentos.index');
	Route::get('/Paquetes_has_Descuentos/{id}/edit','App\Http\Controllers\Paquetes_has_DescuentosController@edit')->name('Paquetes_has_Descuentos.edit');
	Route::put('/Paquetes_has_Descuentos/{id}','App\Http\Controllers\Paquetes_has_DescuentosController@update')->name('Paquetes_has_Descuentos.update');
	Route::delete('/Paquetes_has_Descuentos/{id}','App\Http\Controllers\Paquetes_has_DescuentosController@destroy')->name('Paquetes_has_Descuentos.destroy');
	
});

Route::group(['middleware' => ['role:Usuario']], function () { 

	//Galeria
	Route::post('/galery', 'App\Http\Controllers\GaleriaController@store')->name('Galeria.store');
	Route::get('/galery', 'App\Http\Controllers\GaleriaController@index')->name('Galeria.index');
	Route::get('/galery/{id}/edit','App\Http\Controllers\GaleriaController@edit')->name('Galeria.edit');
	Route::put('/galery/{id}','App\Http\Controllers\GaleriaController@update')->name('Galeria.update');
	Route::delete('/galery/{id}','App\Http\Controllers\GaleriaController@destroy')->name('Galeria.destroy');
	Route::get('/galery', 'App\Http\Controllers\GaleriaController@galery')->name('Galeria.index');


	//Reglas
	Route::post('/Reglas', 'App\Http\Controllers\ReglasController@store')->name('Reglas.store');
	Route::get('/Reglas', 'App\Http\Controllers\ReglasController@index')->name('Reglas.index');
	Route::get('/Reglas/{id}/edit','App\Http\Controllers\ReglasController@edit')->name('Reglas.edit');
	Route::put('/Reglas/{id}','App\Http\Controllers\ReglasController@update')->name('Reglas.update');
	Route::delete('/Reglas/{id}','App\Http\Controllers\ReglasController@destroy')->name('Reglas.destroy');

		
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
	Route::get('{carpeta}', ['as' => 'page.carpeta', 'uses' => 'App\Http\Controllers\PageController@carpeta']);
});

